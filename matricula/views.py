from django.shortcuts import render, redirect, HttpResponse, get_object_or_404
from django.http import JsonResponse
import json
from django.core import serializers
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.messages import error
from django.db.models import Q, F, Value
from django.db.models.functions import Concat
from .models import Categoria_Aluno, Aluno, Turma, Curso, Matricula, Tipo_matricula, Status_matricula, Departamento_teste, \
Departamento, Disciplina, Calendario
import pandas as pd
import os
from django.apps import apps
from .functions import registro_user_isvalid, valid_register, check_cpf, cpf_is_valid, try_user, valid_matricula_fields, valid_matricula_files
import datetime
from .constants import dicionario_colunas, colunas_final, colunas_inicio, dict_calendario
from django.views.generic import CreateView
from django import forms
from django.conf import settings
from django.http import FileResponse


def index(request):
    return redirect('calendario')

def logar(request):
    if request.user.is_authenticated:
        return redirect('solicitar_matricula')
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect("index")
        else:
            error(request, "Usuário e/ou senha incorretos")

    return render(request, 'matricula/login.html')

def deslogar(request):
    logout(request)
    return redirect("calendario")

def registrar_usuario(request):
    if request.user.is_authenticated:
        return redirect('solicitar_matricula')
    if request.method == "POST":
        cpf = request.POST.get('text_cpf')
        err = None
        content = None
        if cpf_is_valid(cpf):
            student = Aluno().get_aluno_cpf(cpf)
        else:
            err = 'CPF inválido.'
        if student is None:
            # cria o aluno com categoria e cpf
            content = {'category' : 'Aluno de fora', 'cpf' : cpf}
        else:
            if student.user is None:
                content = {'category' : 'Em Curso', 'student' : student, 'cpf' : cpf}
            else:
                err = 'Aluno já cadastrado.'
        if err is not None:
            error(request, err)
            return redirect('registrar_usuario')
        return render(request, 'matricula/registrar_aluno.html', content)
    return render(request, 'matricula/registrar_usuario.html')

def registrar_aluno(request):
    if request.user.is_authenticated:
        return redirect('solicitar_matricula')
    if request.method == "POST":
        cpf = request.POST.get('text_cpf')
        student = Aluno().get_aluno_cpf(cpf)
        username = request.POST.get('text_username')
        password = request.POST.get('text_password')
        re_password = request.POST.get('text_re_password')
        name = request.POST.get('text_name')
        rg = request.POST.get('text_rg')
        ra = request.POST.get('text_ra')
        phone = request.POST.get('text_telefone')
        email = request.POST.get('text_email')
        category = request.POST.get('text_category')
        course = request.POST.get('text_curso')
        university = request.POST.get('text_universidade')
        err, msg, aluno = valid_register(username, password, re_password, cpf, name, rg, ra, phone, email, university, \
        course, category, student)
        if err is not None:
            error(request, msg)
            content = {'student' : student, 'cpf' : cpf, 'category' : category}
            return render(request, 'matricula/registrar_aluno.html', content)
        else:
            user = authenticate(request, username=username, password=password)
            login(request, user)
            return redirect('solicitar_matricula')
    return render(request, 'matricula/registrar_usuario.html')

@login_required
def matricular(request):
    if try_user(request) != "aluno":
        return redirect('calendario')
    aluno = request.user.aluno
    current_semester = Calendario().get_current_semester()
    active_ae = Calendario().is_active_ae()
    active_aj = Calendario().is_active_aj()
    active_eq = Calendario().is_active_eq()

    if request.method == "POST":
        turma_field = request.POST.get("turma")
        disciplina_field = request.POST.get("disciplina")
        tipo = request.POST.get("tipo_matricula")
        justificativa = request.POST.get("justificativa")
        err = ""
        if not valid_matricula_fields(turma_field, disciplina_field, tipo, justificativa):
            err = "Preencha todos os campos."
        turma = Turma.objects.get(pk=turma_field) if turma_field else \
                Turma.objects.get(pk=disciplina_field)
        try:
            rg_file = request.FILES['rg_file']
            comp_file = request.FILES['comp_file']
            err = valid_matricula_files(rg_file, comp_file)
        except:
            rg_file = ""
            comp_file = ""         
        if err == "":
            nova_matricula = Aluno.matricular(aluno, turma, tipo, justificativa, rg_file, comp_file)
            if nova_matricula == 1:
                erro = "Matrícula na disciplina %s já solicitada." % turma
                error(request, erro)
            return render(request, "matricula/matricula.html", {'nova_matricula': nova_matricula, 'current_semester' : current_semester,
            'active_ae' : active_ae, 'active_aj' : active_aj, 'active_eq' : active_eq})
        else:
            error(request, err)
    return render(request, "matricula/matricula.html", {'current_semester' : current_semester, 'active_ae' : active_ae, 
    'active_aj' : active_aj, 'active_eq' : active_eq})

@login_required
def consultar_matricula(request):
    try:
        aluno = request.user.aluno
    except:
        try:
            admin = request.user.admin
            return redirect('processar_matriculas')
        except: # não é aluno nem admin, redirecionar pra algum lugar
            return HttpResponse("Erro com o usuário '%s', entre em contato com o administrador." %request.user.username)
    id_cancelada = Status_matricula().get_id_cancelada()
    matriculas = Matricula.objects.filter(aluno=aluno).exclude(status_id=id_cancelada).order_by('turma__curso')
    print(matriculas)
    if not matriculas.exists():
        error(request, "Nenhuma matrícula encontrada")
    return render(request, "matricula/consultar_matricula.html", {'matriculas': matriculas})

@login_required
def excluir_matricula(request):
    try:
        aluno = request.user.aluno
    except:
        try:
            admin = request.user.admin
            return redirect('processar_matriculas')
        except: # não é aluno nem admin, redirecionar pra algum lugar
            return HttpResponse("Erro com o usuário '%s', entre em contato com o administrador." %request.user.username)
    if request.method == "POST":
        matricula_id = request.POST.get('delete_button')
        id_cancelada = Status_matricula().get_id_cancelada()
        matricula = Matricula.objects.filter(pk=matricula_id)
        if matricula[0].aluno_id == aluno.id:
            matricula.update(status_id = id_cancelada)
        # todo: colocar verificação se a matrícula a ser excluída é do aluno
    return redirect('consultar_matricula')
    # todo: retornar alerta se deseja realmente excluir e, após, excluir do bd e atualizar a página via ajax (?) ou não

@login_required
def detalhe_matricula(request):
    try:
        aluno = request.user.aluno
    except:
        try:
            admin = request.user.admin
            return redirect('processar_matriculas')
        except: # não é aluno nem admin, redirecionar pra algum lugar
            return HttpResponse("Erro com o usuário '%s', entre em contato com o administrador." %request.user.username)
    matricula_id = request.POST.get('action')
    matricula = Matricula.objects.get(pk=matricula_id)
    return render(request, "matricula/detalhe_matricula.html", {'matricula': matricula})

@login_required
def processar_matriculas(request):
    if try_user(request) != 'admin':
        return redirect('calendario')
    if request.method == "POST":
        check_colunas = request.POST.getlist('checkbox_exibir_coluna')
        lista_colunas = list()
        lista_colunas.extend(colunas_inicio)
        for coluna in check_colunas:
            lista_colunas.append(dicionario_colunas[coluna])
        lista_colunas.extend(colunas_final)
        tipo_id = request.POST.get('tipo')
        tipo = ""
        if tipo_id != "":
            tipo = Tipo_matricula.objects.filter(pk=tipo_id).values_list('tipo', flat=True)[0]
        status_id = request.POST.get('status')
        status = ""
        if status_id != "":
            status = Status_matricula.objects.filter(pk=status_id).values_list('status', flat=True)[0]
        calendario_id = request.POST.get('sel_semesters')
        try:
            calendario = get_object_or_404(Calendario, pk=calendario_id)
            ano = calendario.ano
            semestre = calendario.semestre
        except:
            ano = ""
            semestre = ""
        curso_disciplina = request.POST.get('curso_disciplina')
        disciplina = request.POST.get('text_disciplina')
        atendidas = request.POST.get('show_atendidas')
        if request.user.admin.categoria == "depto":
            cod_depto = request.user.username
            departamento = Departamento.objects.get(pk=cod_depto).nome
        else:
            departamento = request.POST.get('text_departamento')
        aluno = request.POST.get('text_aluno')
        curso_aluno = request.POST.get('text_curso_aluno')
        matriculas = Matricula.objects.annotate(
            alias_turma = Concat(F('turma__disciplina__codigo'), F('turma__turma'), Value(' - '), F('turma__disciplina__nome')),
        )
        id_cancelada = Status_matricula().get_id_cancelada()
        matriculas = matriculas.filter(
            (Q(aluno__nome__icontains=aluno) | Q(aluno__ra__icontains=aluno)) & Q(aluno__curso__icontains=curso_aluno) &
            Q(tipo__tipo__icontains=tipo) & Q(turma__curso__codigo__icontains=curso_disciplina) &
            (Q(turma__disciplina__codigo__icontains=disciplina) | Q(turma__disciplina__nome__icontains=disciplina)) &
            (Q(turma__disciplina__departamento__codigo__icontains=departamento) | 
            Q(turma__disciplina__departamento__nome__icontains=departamento)) & Q(status__status__icontains=status) &
            Q(turma__calendario__ano__icontains=ano) & Q(turma__calendario__semestre__icontains=semestre)
        )
        if atendidas is None:
            matriculas = matriculas.exclude(atendida=1)

        if status_id != str(id_cancelada):
            matriculas = matriculas.exclude(status_id=id_cancelada)

        matriculas = matriculas.values(*lista_colunas)
        ids = list()
        for matricula in matriculas:
            ids.append(matricula['id'])
        if not(matriculas.exists()):
            erro = "Nenhuma matrícula encontrada"
            error(request, erro)
        return render(request, "ajax/load_matriculas.html", {'matriculas' : matriculas, 'ids' : ids})
    return render(request, "matricula/processar_matriculas.html")

@login_required
def editar_matricula(request, id_matricula):
    try:
        admin = request.user.admin
    except:
        try:
            aluno = request.user.aluno
            return redirect('solicitar_matricula')
        except:
            return HttpResponse("Erro com o usuário '%s', entre em contato com o administrador." %request.user.username)
    try:
        matricula = get_object_or_404(Matricula, pk=id_matricula)
        if request.method == "POST":
            status = request.POST.get('status')
            parecer = request.POST.get('text_parecer')
            observacao = request.POST.get('text_observacao')
            matricula.status_id = status
            matricula.parecer_docente = parecer
            matricula.observacao_graduacao = observacao
            matricula.save()
            return redirect("processar_matriculas")
    except:
        erro = "Nenhuma matrícula encontrada"
        matricula = None
        error(request, erro)
    return render(request, "matricula/editar_matricula.html", {'matricula': matricula})

def show_calendario(request):
    calendario = Calendario().get_current_semester()
    return render(request, 'matricula/calendario.html', {'c' : calendario})

@login_required
def atualizar_bd(request):
    if try_user(request) != 'admin':
        return redirect('calendario')
    if request.method == "POST":
        tipo = request.POST.get('radio_tipo')
        tabela = request.POST.get('radio_tabela')
        if tipo == '' or tabela == '':
            return render(request, 'matricula/atualizar_bd.html')
        elif tipo != "delete_all" and "upload_file" not in request.FILES:
            return render(request, 'matricula/atualizar_bd.html')
        elif tipo != "delete_all":
            file = request.FILES['upload_file']
        else:
            file = None
        Modelo = apps.get_model('matricula', tabela)
        
        m = Modelo()
        Modelo.update_model(self=m, file=file, tipo=tipo)
    return render(request, 'matricula/atualizar_bd.html')

@login_required
def load_editar_matricula(request, id_matricula):
    try:
        matricula = get_object_or_404(Matricula, pk=id_matricula)
        if request.method == "POST":
            status = request.POST.get('status')
            parecer = request.POST.get('text_parecer')
            observacao = request.POST.get('text_observacao')
            matricula.status_id = status
            matricula.parecer_docente = parecer
            matricula.observacao_graduacao = observacao
            matricula.save()
            return redirect("processar_matriculas")
    except:
        erro = "Nenhuma matrícula encontrada"
        matricula = None
        error(request, erro)
    return render(request, "ajax/load_editar_matricula.html", {'matricula': matricula})

@login_required
def delete_matricula_ajax(request, id_matricula):
    matricula = Matricula.objects.filter(pk=id_matricula)
    if matricula.exists():
        id_cancelada = Status_matricula().get_id_cancelada()
        matricula.update(status_id=id_cancelada)
    return redirect("processar_matriculas")

@login_required
def update_atendida_ajax(request, id_matricula, value):
    matricula = Matricula.objects.filter(pk=id_matricula)
    if matricula.exists():
        matricula.update(atendida=value)
    return JsonResponse(serializers.serialize('json', matricula), safe=False)

@login_required
def batch_update(request):
    ids_string = request.POST.get('ids')
    status = request.POST.get('status')
    ids_int = []
    for id_int in ids_string.split(','):
        ids_int.append(id_int)
    matriculas = Matricula.objects.filter(id__in=ids_int)
    matriculas.update(status_id=status)
    return JsonResponse(serializers.serialize('json', matriculas), safe=False)

@login_required
def load_turmas(request): #pegar as turmas P do curso
    turma_t_id = request.GET.get("var_pai")
    turma_t = Turma.objects.get(pk=turma_t_id)
    disciplina = turma_t.disciplina
    turmas = Turma.objects.filter(disciplina=disciplina, tipo="P")
    return render(request, 'ajax/load_turmas.html', {'turmas': turmas})

@login_required
def load_cursos(request):
    tipo = request.GET.get("var_pai")
    fcav = Curso().get_todos_cursos_fcav()
    if tipo == "todos":
        cursos = fcav.all()
    else:
        categoria = request.user.aluno.categoria.categoria
        if tipo == "AE":  #AE
            cursos = fcav.exclude(nome=request.user.aluno.curso) if categoria == "Em Curso" else fcav.all()
        elif tipo == "AJ" and categoria == "Em Curso":  #Ajuste
            cursos = fcav.filter(nome=request.user.aluno.curso)
        elif tipo == "EQ" and categoria == "Em Curso":  #Equivalentes
            cursos = fcav.exclude(nome=request.user.aluno.curso)
    if cursos:
        return render(request, 'ajax/load_cursos.html', {'cursos': cursos})
    else:
        return redirect("solicitar_matricula")

@login_required
def load_disciplinas(request): #pegar as turmas T e TP do curso;
    curso_id = request.GET.get("var_pai")
    curso = Curso.objects.get(codigo=curso_id)
    turmas = Turma().get_turmas_curso(curso=curso).order_by('disciplina__nome')
    turmas = turmas.exclude(tipo="P")
    return render(request, 'ajax/load_disciplinas.html', {'disciplinas': turmas})

@login_required
def load_tipo_matricula(request):
    tipos = Tipo_matricula.objects.all()
    return render(request, 'ajax/load_tipo_matricula.html', {'tipos': tipos})

@login_required
def load_categoria_aluno(request):
    categorias = Categoria_Aluno.objects.all().exclude(categoria="Em Curso")
    return render(request, 'ajax/load_categoria_aluno.html', {'categorias': categorias})

@login_required
def load_status_matricula(request):
    all_status = Status_matricula.objects.all()
    return render(request, 'ajax/load_status_matricula.html', {'all_status': all_status})


@login_required
def load_semesters(request):
    semesters = Calendario().get_all_semestres().order_by('-ano', '-semestre')
    return render(request, 'ajax/load_semesters.html', {'semesters' : semesters})

@login_required
def report_departamentos(request):
    if try_user(request) != 'admin':
        return redirect('calendario')
    departamentos = Departamento().get_all_departamentos()
    return render(request, 'relatorios/departamentos.html', {'departamentos' : departamentos})

@login_required
def report_disciplinas(request):
    if try_user(request) != 'admin':
        return redirect('calendario')
    depto = request.user.username if request.user.admin.categoria == "depto" else ""
    disciplinas = Disciplina().get_disciplinas_depto(depto).order_by('departamento', 'nome')
    return render(request, 'relatorios/disciplinas.html', {'disciplinas' : disciplinas})

@login_required
def report_turmas(request):
    if try_user(request) != 'admin':
        return redirect('calendario')
    depto = request.user.username if request.user.admin.categoria == "depto" else ""
    turmas = Turma().get_turmas_depto(depto).order_by('disciplina__nome', 'turma')
    return render(request, 'relatorios/turmas.html', {'turmas' : turmas})

@login_required
def report_alunos(request):
    if try_user(request) != 'admin':
        return redirect('calendario')
    alunos = Aluno().get_all_alunos().order_by('nome')
    return render(request, 'relatorios/alunos.html', {'alunos' : alunos})

@login_required
def report_alunos_fora(request):
    if try_user(request) != 'admin':
        return redirect('calendario')
    alunos = Aluno().get_alunos_fora().order_by('nome')
    return render(request, 'relatorios/alunos_fora.html', {'alunos' : alunos})


@login_required
def download_file(request, file_name):
    if try_user(request) != 'admin':
        return redirect('calendario')   
    file_path = os.path.join(settings.MEDIA_ROOT, file_name)
    try:
        fl = open(file_path, 'rb')
        response = FileResponse(fl)
    except:
        err = "Arquivo não encontrado."
        error(request, err)
        return redirect('relatorio_alunos_fora')
    return response

@login_required
def teste_upload(request):
    if request.method == "POST":
        aluno = Aluno.objects.get(user=request.user)
        rg_file = request.FILES['rg_file']
        comp_file = request.FILES['comp_file']
        print(rg_file.content_type)
        if rg_file.content_type != "application/pdf" or comp_file.content_type != "aplication/pdf":
            error(request, "Anexe arquivos no formato PDF.")
        elif rg_file is None or comp_file is None:
            error(request, "Anexe todos os arquivos.")
        else:
            aluno.rg_file = rg_file
            aluno.comprovante_file = comp_file
            aluno.save()
            return render(request, 'matricula/teste.html', {'aluno' : aluno})
    return render(request, 'matricula/teste.html')