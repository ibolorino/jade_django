from django.urls import path
from . import views
from django.contrib.auth import views as auth_views
from django.conf import settings
from .tokens import user_tokenizer


urlpatterns = [
    path('', views.index, name="index"),
    path('login/', views.logar, name='login'),
    path('registrar_usuario/', views.registrar_usuario, name='registrar_usuario'),
    path('registrar_aluno/', views.registrar_aluno, name='registrar_aluno'),
    path('logout/', views.deslogar, name='logout'),
    path('solicitar_matricula/', views.matricular, name="solicitar_matricula"),
    path('consultar_matricula/', views.consultar_matricula, name="consultar_matricula"),
    path('excluir_matricula', views.excluir_matricula, name="excluir_matricula"),
    path('detalhe_matricula', views.detalhe_matricula, name="detalhe_matricula"),
    path('sistema/matriculas', views.processar_matriculas, name="processar_matriculas"),
    path('sistema/matriculas/editar/<int:id_matricula>', views.editar_matricula, name='editar_matricula'),
    path('sistema/atualizar_bd', views.atualizar_bd, name="atualizar_bd"),
    path('sistema/relatorios/departamentos', views.report_departamentos, name='relatorio_departamentos'),
    path('sistema/relatorios/disciplinas', views.report_disciplinas, name='relatorio_disciplinas'),
    path('sistema/relatorios/turmas', views.report_turmas, name='relatorio_turmas'),
    path('sistema/relatorios/alunos', views.report_alunos, name='relatorio_alunos'),
    path('sistema/relatorios/alunos_fora', views.report_alunos_fora, name='relatorio_alunos_fora'),
    path('sistema/resetar_senha', auth_views.PasswordResetView.as_view(
        template_name="sistema/reset_password.html",
        html_email_template_name='sistema/reset_password_email.html',
        subject_template_name='sistema/subject_reset_password.txt',
        success_url=settings.LOGIN_URL,
        token_generator=user_tokenizer,
    ), name="reset_password"),
    path('reset-password-confirmation/<str:uidb64>/<str:token>/', auth_views.PasswordResetConfirmView.as_view(
        template_name='sistema/reset_password_update.html', 
        post_reset_login=True,
        post_reset_login_backend='django.contrib.auth.backends.ModelBackend',
        token_generator=user_tokenizer,
        success_url=settings.LOGIN_REDIRECT_URL
    ),name='password_reset_confirm'),
    path('calendario', views.show_calendario, name="calendario"),
    path('ajax/load_turmas', views.load_turmas, name="load_turmas"),
    path('ajax/load_cursos', views.load_cursos, name="load_cursos"),
    path('ajax/load_disciplinas', views.load_disciplinas, name="load_disciplinas"),
    path('ajax/load_tipo_matricula', views.load_tipo_matricula, name="load_tipo_matricula"),
    path('ajax/load_categoria_aluno', views.load_categoria_aluno, name="load_categoria_aluno"),
    path('ajax/load_status_matricula', views.load_status_matricula, name="load_status_matricula"),
    path('ajax/load_semesters', views.load_semesters, name="load_semesters"),
    path('ajax/load_editar_matricula/<int:id_matricula>', views.load_editar_matricula, name="load_editar_matricula"),
    path('ajax/delete_matricula/<int:id_matricula>', views.delete_matricula_ajax, name="delete_matricula_ajax"),
    path('ajax/update_atendida/<int:id_matricula>/<int:value>', views.update_atendida_ajax, name="update_atendida"),
    path('ajax/batch_update', views.batch_update, name="batch_update"),
    path('download/<path:file_name>', views.download_file, name="download_file"),
    path('teste', views.teste_upload, name='teste_upload'),
]