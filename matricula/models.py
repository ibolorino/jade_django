from django.db import models
from django.contrib.auth.models import User
import datetime
from .db_update import update_db, handle_turma, handle_aluno
import pandas as pd
from django.shortcuts import get_object_or_404




class Administrador(models.Model):
    nome = models.CharField("Nome", max_length=255)
    categoria = models.CharField("Categoria", max_length=20)
    user = models.OneToOneField(User, related_name="admin", on_delete=models.CASCADE)

    def __str__(self):
        return self.nome



class Aluno(models.Model):

    def get_rg_path(self, filename):
        cs = Calendario().get_current_semester()
        ext = filename.split('.')[-1]
        arquivo = "{0}/{1}/RG-{2}.{3}".format(cs.ano, cs.semestre, self.user_id, ext)
        return arquivo

    def get_comp_path(self, filename):
        cs = Calendario().get_current_semester()
        ext = filename.split('.')[-1]
        arquivo = "{0}/{1}/COMP-{2}.{3}".format(cs.ano, cs.semestre, self.user_id, ext)
        return arquivo

    nome = models.CharField("Nome", max_length=255, blank=True, null=True)
    ra = models.CharField("R.A.", max_length=20, blank=True, null=True)
    rg = models.CharField("R.G.", max_length=20, blank=True, null=True)
    cpf = models.CharField("CPF", max_length=11)
    telefone = models.CharField("Telefone", max_length=20, null=True, blank=True)
    email = models.CharField("Email", max_length=255, null=True, blank=True)
    universidade = models.CharField("Universidade", max_length=255, blank=True, null=True)
    curso = models.CharField("Curso", max_length=255, blank=True, null=True)
    cr = models.DecimalField("Coeficiente de Rendimento", max_digits=5, decimal_places=4, blank=True, null=True)
    cm = models.DecimalField("Coeficiente de Matrícula", max_digits=5, decimal_places=4, blank=True, null=True)
    cp = models.DecimalField("Coeficiente de Progressão", max_digits=5, decimal_places=4, blank=True, null=True)
    mp = models.DecimalField("Média Ponderada", max_digits=5, decimal_places=4, blank=True, null=True)
    categoria = models.ForeignKey("Categoria_Aluno", models.PROTECT, 'alunos_categoria', default=1, blank=True, null=True)
    rg_file = models.FileField("Arquivo RG", upload_to=get_rg_path, blank=True, null=True)
    comprovante_file = models.FileField("Arquivo Comprovante", upload_to=get_comp_path, blank=True, null=True)
    file_upload_date = models.DateTimeField("Data do upload dos arquivos", blank=True, null=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="aluno", null=True, blank=True)

    def __str__(self):
        if self.nome is None:
            return self.cpf
        return self.nome

    def get_aluno_cpf(self, cpf):
        try:
            aluno = get_object_or_404(self.__class__, cpf=cpf)
        except:
            aluno = None
        return aluno

    def matricular(self, turma, tipo, justificativa, rg_file, comprovante_file):
        data_solicitacao = datetime.datetime.now()
        s = Status_matricula()
        status = s.get_status("Em análise")
        atendida = False
        id_cancelada = Status_matricula().get_id_cancelada()
        if Matricula.objects.filter(turma=turma, aluno=self).exclude(status_id=id_cancelada):
            nova_matricula = 1  # já matriculado
        else:
            nova_matricula = Matricula(turma=turma, aluno=self, tipo_id=tipo, data_solicitacao=data_solicitacao.date(),
                                   justificativa=justificativa, status=status, atendida=atendida)
            nova_matricula.save()
            if rg_file != "" and comprovante_file != "":
                self.rg_file = rg_file
                self.comprovante_file = comprovante_file
                self.file_upload_date = data_solicitacao
                self.save()
        return nova_matricula

    def create_student(self, fields_student):
        student = Aluno.objects.create(**fields_student)
        return student

    def get_all_alunos(self):
        return Aluno.objects.all()

    def get_alunos_fora(self):
        return Aluno.objects.exclude(universidade="FCAV")

    def update_model(self, file, tipo):
        handle_aluno(self, file, tipo)
        # if tipo != "delete_all":
        #     handle_aluno(self, file, tipo)
        # else:
        #     update_db(self, file, tipo)
        # return
        


class Categoria_Aluno(models.Model):
    categoria = models.CharField("Categoria", max_length=255)

    def __str__(self):
        return self.categoria

    def get_categorias(self):
        return Categoria_Aluno.objects.all()



class Curso(models.Model):
    codigo = models.CharField("Código", max_length=10, primary_key=True)
    nome = models.CharField("Nome do Curso", max_length=255)
    universidade = models.ForeignKey("Universidade", models.PROTECT, related_name="cursos")

    def __str__(self):
        return self.nome + " (" + self.universidade.nome + ")"

    def get_todos_cursos_fcav(self):
        cursos = Curso.objects.filter(universidade=Universidade.objects.get(nome="FCAV"))
        return cursos

    def get_curso_fcav(self, codigo_fcav):
        cursos = Curso.objects.get(universidade=Universidade.objects.get(nome="FCAV"), codigo=codigo_fcav)
        return cursos



class Universidade(models.Model):
    choices_estado = [
        ('SP', 'São Paulo'),
        ('RJ', 'Rio de Janeiro'),
        ('MG', 'Minas Gerais'),
    ]
    nome = models.CharField("Nome", max_length=255)
    cidade = models.CharField("Cidade", max_length=255)
    estado = models.CharField("Estado", choices=choices_estado, max_length=2, default="SP")

    def __str__(self):
        return self.nome + " - " + self.cidade + "/" + self.estado



class Departamento(models.Model):
    codigo = models.CharField("Código", max_length=10, primary_key=True)
    nome = models.CharField("Nome do Departamento", max_length=255)

    def __str__(self):
        return self.nome


    def update_model(self, file, tipo):
        update_db(self, file, tipo)
        return


    def get_all_departamentos(self):
        return Departamento.objects.all()


class Departamento_teste(models.Model):
    codigo = models.CharField("Código", max_length=10, primary_key=True)
    nome = models.CharField("Nome do Departamento", max_length=255)

    def __str__(self):
        return self.nome

    def update_model(self, file, tipo):
        update_db(self, file, tipo)
        return



class Disciplina(models.Model):
    codigo = models.CharField("Código", max_length=20, primary_key=True)
    nome = models.CharField("Nome da Disciplina", max_length=255)
    carga_horaria = models.PositiveSmallIntegerField("Carga Horária (horas)")
    departamento = models.ForeignKey("Departamento", models.PROTECT, related_name="disciplinas")

    def __str__(self):
        return self.codigo + " - " + self.nome

    def update_model(self, file, tipo):
        update_db(self, file, tipo)
        return

    def get_disciplinas_depto(self, depto):
        return Disciplina.objects.filter(departamento__codigo__icontains=depto)



class Calendario(models.Model):
    semestre = models.PositiveSmallIntegerField("Semestre")
    ano = models.PositiveIntegerField("Ano")
    inicio_semestre = models.DateTimeField("Início do Semestre", null=True)
    fim_semestre = models.DateTimeField("Término do Semestre", null=True)
    inicio_ae = models.DateTimeField("Início Aluno Especial", null=True)
    fim_ae = models.DateTimeField("Término Aluno Especial", null=True)
    inicio_aj = models.DateTimeField("Início Ajustes", null=True)
    fim_aj = models.DateTimeField("Término Ajustes", null=True)
    inicio_eq = models.DateTimeField("Início Equivalentes", null=True)
    fim_eq = models.DateTimeField("Término Equivalentes", null=True)

    def __str__(self):
        return str(self.semestre) + "º semestre de " + str(self.ano)

    def get_all_semestres(self):
        return Calendario.objects.all()

    def get_current_semester(self):
        now = datetime.datetime.now()
        try: 
            current_semester = get_object_or_404(Calendario, inicio_semestre__lte=now, fim_semestre__gte=now)
        except:
            current_semester = Calendario.objects.order_by('-ano', '-semestre')[0]
        return current_semester

    def is_active_ae(self):
        now = datetime.datetime.now()
        active_ae = Calendario.objects.filter(inicio_ae__lte=now, fim_ae__gte=now)
        return active_ae.exists()

    def is_active_aj(self):
        now = datetime.datetime.now()
        active_aj = Calendario.objects.filter(inicio_aj__lte=now, fim_aj__gte=now)
        return active_aj.exists()

    def is_active_eq(self):
        now = datetime.datetime.now()
        active_eq = Calendario.objects.filter(inicio_eq__lte=now, fim_eq__gte=now)
        return active_eq.exists()



class Turma(models.Model):
    choices_tipo = [
        ('TP', 'Teórico-prática'),
        ('T', 'Teórica'),
        ('P', 'Prática'),
    ]
    turma = models.CharField("Turma", max_length=10)
    calendario = models.ForeignKey("Calendario", models.PROTECT, related_name="turmas_semestre")
    disciplina = models.ForeignKey("Disciplina", models.PROTECT, related_name="turmas_disciplina")
    curso = models.ForeignKey("Curso", models.PROTECT, related_name="turmas_curso")
    tipo = models.CharField("Tipo", choices=choices_tipo, max_length=2, default="TP")
    #turma_teorica VER COMO IMPLEMENTA RELACIONAMENTO COM ELA MESMA, SOMENTE PARA TURMAS DO TIPO T

    def __str__(self):
        return self.disciplina.codigo + self.turma + " - " + self.disciplina.nome

    def get_turmas_curso(self, curso): #filtrar por curso após teste
        turmas = Turma.objects.filter(curso=curso)
        return turmas

    def get_turmas_disciplina(self, disciplina):
        turmas = Turma.objects.filter(disciplina=disciplina).order_by("turma")
        return turmas

    def get_turmas_depto(self, depto):
        return Turma.objects.filter(disciplina__departamento__codigo__icontains=depto)

    def update_model(self, file, tipo):
        if tipo != "delete_all":
            handle_turma(self, file, tipo, Calendario)
        else:
            update_db(self, file, tipo)
        return



class Matricula(models.Model):
    turma = models.ForeignKey("Turma", models.CASCADE, related_name="matriculas_turma")
    aluno = models.ForeignKey("Aluno", models.PROTECT, related_name="matriculas_aluno")
    tipo = models.ForeignKey("Tipo_matricula", models.PROTECT, related_name="matriculas_tipo")
    data_solicitacao = models.DateField("Data da solicitação")
    data_cancelamento = models.DateField("Data do cancelamento", null=True, blank=True)
    justificativa = models.TextField("Justificativa", max_length=255)
    status = models.ForeignKey("Status_matricula", models.PROTECT, related_name="matriculas_status")
    atendida = models.BooleanField("Processada pela Graduação")
    parecer_docente = models.TextField("Parecer do Docente", max_length=255, null=True, blank=True)
    observacao_graduacao = models.TextField("Observação da Graduação", max_length=255, null=True, blank=True)

    def __str__(self):
        return str(self.turma)



class Tipo_matricula(models.Model):
    codigo = models.CharField("codigo", max_length=2, primary_key=True)
    tipo = models.CharField("Tipo de matrícula", max_length=255)

    def __str__(self):
        return self.tipo



class Status_matricula(models.Model):
    status = models.CharField("Status da matrícula", max_length=255)

    def __str__(self):
        return self.status

    def get_status(self, status):
        return Status_matricula.objects.get(status=status)

    def get_id_cancelada(self):
        return Status_matricula.objects.get(status='Cancelada').id

    def get_id_analise(self):
        return Status_matricula.objects.get(status="Em análise").id