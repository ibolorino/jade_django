#update db
dict_atualiza_tabela = {
    "Departamento" : ["codigo", "nome"],
    "Departamento_teste" : ["codigo", "nome"],
    "Disciplina" : ["codigo", "nome", "carga_horaria", "departamento_id"],
    "Turma" : ["turma", "calendario_id", "disciplina_id", "curso_id", "tipo"],
    "Aluno" : ["nome", "ra", "rg", "cpf", "curso"]
}

dict_excel_aluno_bd = {
    "documento" : "rg",
    "estrutura curricular" : "curso",
    "coef. matricula" : "cm",
    "coef. progressão" : "cp",
    "coef. rendimento" : "cr",
    "média ponderada" : "mp",
}

replace_curso = {
    "18" : "Engenharia Agronômica",
    "19" : "Engenharia Agronômica",
    "26" : "Medicina Veterinária",
    "37" : "Zootecnia",
    "44" : "Ciências Biológicas",
    "45" : "Ciências Biológicas",
    "71" : "Administração",
    "72" : "Administração",
}

#process enrolments
dicionario_colunas = {
    "ra" : "aluno__ra",
    "rg" : "aluno__rg",
    "cpf" : "aluno__cpf",
    "telefone" : "aluno__telefone",
    "email" : "aluno__email",
    "coef_prog" : "aluno__cp",
    "coef_rend" : "aluno__cr",
    "curso_aluno" : "aluno__curso",
    "universidade" : "aluno__universidade",
    "curso_disciplina" : "turma__curso__nome",
    "depto_disciplina" : "turma__disciplina__departamento__nome",
    "parecer" : "parecer_docente",
    "justificativa" : "justificativa",
    "data_solicitacao" : "data_solicitacao",
    "observacao" : "observacao_graduacao",
}

# process enrolments
colunas_inicio = [
    "id",
    "aluno__nome",
    "alias_turma",
    "tipo__tipo",
]

# process enrolments
colunas_final = [
    "status__status",
    "atendida",
]

#calendario
dict_calendario = {
    "inicio_semestre" : "Início do Semestre",
    "fim_semestre" : "Fim do Semestre",
    "inicio_ae" : "Início Aluno Especial",
    "fim_ae" : "Fim Aluno Especial",
    "inicio_aj" : "Início Ajustes",
    "fim_aj" : "Fim Ajustes",
    "inicio_eq" : "Início Equivalentes",
    "fim_eq" : "Fim Equivalentes",
}