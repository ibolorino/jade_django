from .constants import dict_atualiza_tabela, dict_excel_aluno_bd, replace_curso
import pandas as pd
import numpy as np


def update_db(self, file, tipo):
    model_class = self.__class__
    if tipo == "delete_all":
        delete_all(model_class)
        return
    pd_file = pd.read_excel(file)
    df_file = pd.DataFrame(pd_file)
    if campos_validos(model_class, df_file):
        update_table(Modelo=model_class, df_file=df_file, tipo=tipo)
    else:
        erro = "Colunas inválidas no arquivo do Departamento. Tente novamente"
    return



def update_table(Modelo, df_file, tipo):
    if tipo == "replace_all" or tipo == "add_all":
        if tipo == "replace_all":
            delete_all(Modelo)
        add_records_bd(Modelo, df_file)
    elif tipo == "delete_all":
        delete_all(Modelo)
    elif tipo == "add_update_all":
        pass
        #atualiza os antigos e add os novos
    return



def add_records_bd(Modelo, df_file):
    bulk_model = []
    for i, linha in df_file.iterrows():
        m = Modelo()
        campos = dict_atualiza_tabela[Modelo.__name__]
        for campo in campos:
            setattr(m, campo, linha[campo])
        bulk_model.append(m)
    Modelo.objects.bulk_create(bulk_model)
    return

def delete_all(Modelo):
    Modelo.objects.all().delete()
    return


def campos_validos(Modelo, df_file):
    colunas = list(df_file.columns.values)
    campos = dict_atualiza_tabela[Modelo.__name__]
    validos = True
    for campo in campos:
        if campo not in colunas:
            validos = False
            break
    return validos


def handle_turma(self, file, tipo, class_calendario):
    pd_file = pd.read_excel(file)
    df_file = pd.DataFrame(pd_file)
    dict_semestre = dict()
    semestres = class_calendario.get_all_semestres(class_calendario())
    for semestre in semestres:
        dict_semestre[semestre.id] = [semestre.ano, semestre.semestre]
    df_file = df_file.reindex(columns = np.append( df_file.columns.values, ['calendario_id']))
    for i, row in df_file.iterrows():
        calendario = [row['ano'], row['semestre']]
        for key, value in dict_semestre.items():
            if calendario == dict_semestre[key]:
                df_file.at[i, 'calendario_id'] = key
    model_class = self.__class__
    if campos_validos(model_class, df_file):
        update_table(Modelo=model_class, df_file=df_file, tipo=tipo)
    else:
        print('campos inválidos')
    return 


def handle_aluno(self, file, tipo):
    pd_file = pd.read_excel(file)
    df_file = pd.DataFrame(pd_file)
    df_file.columns = map(str.lower, df_file.columns)
    df_file = df_file.astype({"cpf": str})
    df_file.rename(columns=dict_excel_aluno_bd, inplace=True)
    for i , row in df_file.iterrows():
        df_file.at[i, 'cpf'] = fill_cpf(row['cpf'])
        df_file.at[i, 'curso'] = replace_curso[df_file.at[i, 'curso'][:2]]
    model_class = self.__class__
    if campos_validos(model_class, df_file):
        update_table(model_class, df_file, tipo)
    return
    #continuar com atualizacao dos coeficientes

def fill_cpf(cpf):
    if len(cpf) < 11:
        cpf = fill_cpf("0%s" % cpf)
    return cpf


