
from django.shortcuts import get_object_or_404
from django.contrib.auth.models import User
from .models import Aluno, Categoria_Aluno
import pandas as pd
from django.contrib.auth import login


def registro_user_isvalid(username, password, re_password, cpf, email):
    users = User.objects.all().values('username', 'email')
    df_users = pd.DataFrame(users)
    alunos = Aluno.objects.all().values('cpf', 'user__username')
    df_alunos = pd.DataFrame(alunos)
    for row, value in df_users.iterrows():
        if username == value['username']:
            return ("Usuário já cadastrado.", None)
        if email == value['email']:
            return ("Email já está em uso.", None)
    for row, value in df_alunos.iterrows():
        if cpf == value['cpf'] and value['user__username'] is not None:
            return("Aluno já cadastrado", None)
    if password != re_password:
        return ("Senhas diferentes", None)
    aluno = Aluno.objects.filter(cpf=cpf)
    return ("Ok", aluno)

def user_already_exists(username, df_users):
    for row, value in df_users.iterrows():
        if username == value['username']:
            return True
    return False

def same_passwords(password, re_passowrd):
    return password == re_passowrd

def student_has_user(student):
    return student.user is not None

def has_some_empty_field(fields):
    for field in fields:
        if field == '':
            return True
    return False

def category_is_valid(category, student):
    try:
        get_object_or_404(Categoria_Aluno, categoria=category)
    except: 
        return False
    return category == student.categoria.categoria


def valid_register(username, password, re_password, cpf, name, rg, ra, phone, email, university, course, category, student):
    fields = list([username, password, re_password, cpf, name, rg, ra, phone, email, university, course, category])

# checar se email é válido; usar regex

# salvar se for novo aluno;

# atualizar se for aluno já cadastrado;

    users = User.objects.all().values('username', 'email')
    df_users = pd.DataFrame(users)
    if has_some_empty_field(fields): # algum campo está vazio
        return ('err', 'Preencha todos os campos.', None)
    if user_already_exists(username, df_users): # user já cadastrado
        return ('err', 'Usuário já cadastrado.', None)
    if not(same_passwords(password, re_password)): # senhas diferentes
        return ('err', 'Senhas diferentes.', None)
    if not(cpf_is_valid(cpf)): # cpf inválido
        return ('err', 'CPF inválido', None)
    if student is not None: # atualizar
        if not(category_is_valid(category, student)): # categoria inválida
            return ('err', 'Categoria inválida', None)
        if student_has_user(student): # aluno já tem user
            return ('err', 'Aluno já cadastrado', None)
        user = User.objects.create_user(username=username, password=password, email=email)
        user.save()
        student_update = Aluno.objects.filter(pk=student.id)
        student_update.update(nome=name, rg=rg, ra=ra, telefone=phone, email=email, user=user)
    else: # criar
        categoria_id = Categoria_Aluno.objects.get(categoria=category).id
        user = User.objects.create_user(username=username, password=password, email=email)
        user.save()
        fields_student = {
            'nome' : name, 'cpf' : cpf, 'rg' : rg, 'ra' : ra, 'telefone' : phone, 'email' : email, 'universidade' : university,
            'curso' : course, 'categoria_id' : categoria_id, 'user_id' : user.id
        }
        student = Aluno().create_student(fields_student)
    return (None, None, student)



def check_cpf(cpf):
    
    try:
        aluno = get_object_or_404(Aluno, cpf=cpf)
    except:
        aluno = None
    return aluno

def cpf_is_valid(cpf):
    return (cpf.isdigit() and len(cpf) == 11)

def try_user(request):
    try:
        admin = request.user.admin
        return 'admin'
    except:
        try:
            aluno = request.user.aluno
            return 'aluno'
        except:
            return 'erro'

def valid_matricula_fields(turma_field, disciplina_field, tipo, justificativa):
    if (turma_field is None and  disciplina_field is None) or tipo is None or justificativa is None:
        return False
    return True

def valid_matricula_files(rg_file, comp_file):
    if rg_file.content_type != "application/pdf" or comp_file.content_type != "application/pdf":
        return "Anexe arquivos no formato PDF."
    if rg_file.size > 2000000 or comp_file.size >= 2000000:
        return "Anexe arquivos menores que 2mb."
    return ""