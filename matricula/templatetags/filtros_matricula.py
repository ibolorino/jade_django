from django import template

register = template.Library()

@register.filter(name="nome_campo")
#@stringfilter
def nome_campo(field):
    dict_campo = {
        "id" : "ID",
        "aluno__nome" : "Nome do Aluno",
        "aluno__rg" : "RG",
        "aluno__ra" : "RA",
        "aluno__cpf" : "CPF",
        "alias_turma" : "Turma",
        "tipo__tipo" : "Tipo",
        "status__status" : "Status",
        "aluno__telefone" : "Telefone",
        "aluno__email" : "Email",
        "aluno__cp" : "Coef. de Progressão",
        "aluno__cr" : "Coef. de Rendimento",
        "aluno__curso" : "Curso do aluno",
        "aluno__universidade" : "Universidade do aluno",
        "turma__curso__nome" : "Curso da disciplina",
        "turma__disciplina__departamento__nome" : "Departamento",
        "parecer_docente" : "Parecer do Docente",
        "justificativa" : "Justificativa",
        "data_solicitacao" : "Data da solicitação",
        "observacao_graduacao" : "Observação",
        "atendida" : "Atendida",
    }
    return dict_campo[field]

@register.filter(name="c_bool_none")
def c_bool_none(field, arg):
    args = arg.split(',')
    if field == True:
        field = args[0]
    elif field == False:
        field = args[1]
    elif field == None:
        field = args[2]
    return field

@register.filter(name="index")
def index(lista, i):
    return lista[i]