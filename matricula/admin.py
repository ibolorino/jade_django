from django.contrib import admin
from .models import Aluno, Categoria_Aluno, Curso, Universidade, Departamento, Disciplina, Turma, Calendario, \
    Status_matricula, Matricula, Tipo_matricula, Departamento_teste, Administrador

admin.site.register(Aluno)
admin.site.register(Categoria_Aluno)
admin.site.register(Curso)
admin.site.register(Universidade)
admin.site.register(Departamento)
admin.site.register(Departamento_teste)
admin.site.register(Disciplina)
admin.site.register(Turma)
admin.site.register(Calendario)
admin.site.register(Matricula)
admin.site.register(Status_matricula)
admin.site.register(Tipo_matricula)
admin.site.register(Administrador)
