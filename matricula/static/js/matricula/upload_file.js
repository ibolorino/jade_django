$(".custom-file-input").on("change", function() {
    var fileName = $(this).val().split("\\").pop();
    var ext = $(this).val().split(".").pop().toLowerCase();
    var file_type = $(this).data('msg')
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    if (ext != "pdf" && ext != ""){
        alert('Anexe um ' + file_type + ' no formato PDF.');
        $(this).val('');
        $(this).next().html('Excolha o ' + file_type);
    } else if (ext == "") $(this).next().html('Excolha o ' + file_type);
  });

