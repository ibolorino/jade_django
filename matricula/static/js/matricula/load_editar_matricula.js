$("#modal_editar_matricula").on('shown.bs.modal', function(){
    console.log($('#div_status_matricula').attr('url-ajax'));
    var replace_content = true;
    url = "{% url 'load_status_matricula' %}";
    load_data_ajax(null, $('#div_status_matricula #sel_status'), url, function(){choose_select($('#div_status_matricula #sel_status'), {{ matricula.status_id }});}, replace_content);
});


$("#btn_modal_salvar").click(function(){
    console.log($('#div_status_matricula').attr('url-ajax'));
    var replace_content = true;
    url = "{% url 'load_status_matricula' %}";
    load_data_ajax(null, 
        $('#div_status_matricula #sel_status'), 
        url, 
        function(){choose_select($('#div_status_matricula #sel_status'), {{ matricula.status_id }});}, 
        replace_content
        );
    });