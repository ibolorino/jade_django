$('#sel_curso').change(function(){
    load_disciplinas();
});

$('#sel_disciplina').change(function(){
    load_turmas();
});

$('#div_tipo_matricula input[type=radio]').click(function(){
    var url = $('#div_tipo_matricula').attr('url-ajax');
    console.log($(this).val());
    var replace_content = true;
    load_data_ajax($(this).val(), $('#sel_curso'), url, function(){load_disciplinas();}, replace_content);
});