function load_data_ajax($var_pai, $selector_filho, $url_ajax, $callback_success, $replace_content, $callback_complete){

// todo: implementar success e complete como parâmetros e tirar a responsabilidade de popular os selectors

    var url = $url_ajax;
    var var_pai = $var_pai;
    $.ajax({
        url: url,
        data: {'var_pai': var_pai},
        success: function(data){
            if ($replace_content === false){
                $selector_filho.append(data);
            } else {
                $selector_filho.html(data);
            }
            if ($callback_success) $callback_success();
        },
        complete: function(){
            if ($callback_complete) $callback_complete();
        },
    });
}

function load_turmas(){
    var url = $('#sel_disciplina').attr('url-ajax');
    var replace_content = true;
    load_data_ajax($('#sel_disciplina').val(), $('#sel_turma'), url, function(){return;}, replace_content);
}

function load_disciplinas(){
    var url = $('#sel_curso').attr('url-ajax');
    var replace_content = true;
    load_data_ajax($('#sel_curso').val(), $('#sel_disciplina'), url, function(){load_turmas();}, replace_content);

}

function load_matriculas(selector_pai, selector_filho, $callback){
    var url = selector_pai.attr('url-ajax');
    $.ajax({
        type: 'post',
        url: url,
        data: selector_pai.serialize(),
        success: function(data){
            selector_filho.html(data);
            if ($callback) $callback();
        },
    });
}

function delete_matricula(url){
    $.ajax({
        url: url,
        success: function(){
            atualizar_tela();
        },
    });
}

function call_ajax(url, $callback_success, $callback_complete){
    $.ajax({
        url: url,
        dataType: 'json',
        success: function(data){
            if ($callback_success) $callback_success();
        },
        complete: function(data){
            if ($callback_complete) $callback_success();
        },
    });
}

function batch_update(url, data, $callback_success, $callback_complete){
    $.ajax({
        url: url,
        data: data,
        type: 'post',
        dataType: 'json',
        success: function(data){
            if ($callback_success) $callback_success();
        },
        complete: function(data){
            if ($callback_complete) $callback_success();
        },
    });
}

function update_atendida(url){
    $.ajax({
        url: url,
    });
}

function choose_select($selector, id_select){
    $selector.val(id_select);
}

function modal_show(modal){
    modal.modal('show');
}

function salvar_matricula_ajax($form, $callback){
    var url = $form.attr('url-ajax');
    $.ajax({
        type: 'post',
        url: url,
        cache: false,
        data: $form.serialize(),
        success: function(){
            modal_hide($('#modal_editar_matricula'));
            if ($callback) $callback();
        },
    });
}

function atualizar_tela($callback){
    var selector_pai = $('#form_processar_matricula');
    var selector_filho = $('#selector_matriculas');

    load_matriculas(selector_pai, selector_filho, $callback);
}

function modal_hide(modal){
    console.log(modal);
    $('#modal_editar_matricula').on('hidden.bs.modal', function(){
        atualizar_tela();
    });
    modal.modal('hide');
}