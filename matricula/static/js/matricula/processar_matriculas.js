$(document).ready(function(){
    var replace_content = false;
    var url = $('#div_tipo_matricula').attr('url-ajax');
    load_data_ajax(null, $('#sel_tipo'), url, null, replace_content);
    url = $('#sel_curso_disciplina').attr('url-ajax');
    load_data_ajax("todos", $('#sel_curso_disciplina'), url, null, replace_content);
    url = $('#div_status_matricula').attr('url-ajax');
    load_data_ajax(null, $('#sel_status'), url, null, replace_content);
});

$('#bt_pesquisar').click(function(){
    var selector_pai = $('#form_processar_matricula');
    var selector_filho = $('#selector_matriculas');
    load_matriculas(selector_pai, selector_filho, function(){hide_div_pesquisa();});
});

$('#bt_nova_pesquisa').click(function(){
    $('#div_nova_pesquisa').addClass('d-none');
    $('#div_pesquisa').removeClass('d-none');
});


function hide_div_pesquisa(){
    $('#div_nova_pesquisa').removeClass('d-none');
    $('#div_pesquisa').addClass('d-none');
}